__all__ = ["MediaAgent", "__version__", "__name__", "__author__"]

__version__ = "1.1.2"
__name__ = "MediaAgent"
__author__ = "Lukas Riedersberger"
