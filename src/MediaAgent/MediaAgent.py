# -*- coding: utf-8 -*-
from telegram.ext import Updater
from telegram.ext.updater import Bot

from datetime import datetime
import ssl
import threading
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from babel import Locale
from google_images_download import google_images_download
import sqlite3
import os
import shutil
from PIL import Image
import json
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import tmdbsimple as tmdb
import random
from pathlib import Path
from json import JSONDecodeError
from MediaAgent import __version__
from MediaAgent import __name__
from MediaAgent.Movie import Movie
from MediaAgent.Database import Database


def make_custom_handler(database, bot: Bot, movie_channel, movie_telegram_lock, messages):
    class CustomHandler(BaseHTTPRequestHandler):

        def __init__(self, *args, **kwargs):
            super(CustomHandler, self).__init__(*args, **kwargs)

        def _set_headers(self):
            self.send_response(200)
            self.send_header('content-type', 'application/json; charset=utf-8')
            self.end_headers()

        def do_POST(self):
            self._set_headers()
            data_string = self.rfile.read(int(self.headers['Content-Length']))

            data = json.loads(data_string.decode('utf-8'))
            if not database.token_exists(token=data['token']):
                self.send_response(403)
                self.end_headers()
                return

            if data["type"] == "music":
                MediaAgent.handle_album(
                    bot=bot,
                    user=data["user"],
                    band=data["band"],
                    album=data["album"],
                )
            if data["type"] == "movie":
                success = MediaAgent.handle_movie(
                    database=database,
                    movie_channel=movie_channel,
                    movie_telegram_lock=movie_telegram_lock,
                    messages=messages,
                    bot=bot,
                    user=data["user"],
                    movie=Movie.from_dict(data["data"]),
                    publish=data["publish"],
                )
                if not success:
                    self.send_response(503)
                    self.end_headers()
                    return
            self.send_response(200)
            self.end_headers()
            return

    return CustomHandler


class MediaAgent:

    @staticmethod
    def load_config(file):
        filename = Path(file)
        filename.touch(exist_ok=True)
        with open(file, 'r') as json_file:
            try:
                return json.load(json_file)
            except JSONDecodeError as e:
                print(f"JSONDecodeError: {e.msg}")
        return {}

    def __init__(self):
        self.home = str(Path.home())
        self.dir = self.home + "/.mediaagent/"
        Path(self.dir).mkdir(parents=True, exist_ok=True)
        self.config = {}
        random.seed(datetime.now())
        self.search = None
        self.token = None
        self.music_channel = None
        self.movie_channel = None
        self.music_group = None
        self.updater = None

        self.database = None
        # sqlite3
        self.connection = None
        self.cursor = None
        self.client_credentials_manager = None
        self.sp = None
        self.movie_telegram_lock = None
        self.messages = ["{0} hat jetzt"]

    @staticmethod
    def show_release_details(rel):
        """Print some details about a release dictionary to stdout.
        """
        # "artist-credit-phrase" is a flat string of the credited artists
        # joined with " + " or whatever is given by the server.
        # You can also work with the "artist-credit" list manually.
        print("{}, by {}".format(rel['title'], rel["artist-credit-phrase"]))
        if 'date' in rel:
            print("Released {} ({})".format(rel['date'], rel['status']))
        print("MusicBrainz ID: {}".format(rel['id']))

    @staticmethod
    def convertMillis(millis):
        millis = int(millis)
        seconds = (millis / 1000) % 60
        seconds = int(seconds)
        minutes = (millis / (1000 * 60)) % 60
        minutes = int(minutes)
        hours = (millis / (1000 * 60 * 60)) % 24
        hours = int(hours)
        msg = ""
        if hours > 0:
            msg += str(hours) + ":"
        if minutes > 0:
            msg += str(minutes) + ":"
        msg += str(seconds)
        return msg

    @staticmethod
    def get_tracks(spotify, band, album):
        results = spotify.search(q="artist: " + band + " album:" + album, type="album")
        # get the first album uri
        album_id = results['albums']['items'][0]['uri']
        # get album tracks
        tracks = spotify.album_tracks(album_id)
        i = 1
        tracknames = []
        for track in tracks['items']:
            duration_ms = track['duration_ms']
            duration = MediaAgent.convertMillis(duration_ms)
            name = str(i) + ". " + str(track['name']) + " " + duration
            tracknames.append(name)
            i += 1
        return tracknames

    @staticmethod
    def post_new_album(music_channel, spotify, user, band, album, bot: Bot):
        album_cover = MediaAgent.get_album_cover(band, album)

        img = None
        if len(album_cover) > 0:
            basefile, ext = os.path.splitext(album_cover)
            img = Image.open(album_cover)
            baseheight = 256
            hpercent = (baseheight / float(img.size[1]))
            wsize = int((float(img.size[0]) * float(hpercent)))
            img = img.resize((wsize, baseheight), Image.ANTIALIAS)
            resized_filename = basefile + "_resized." + ext
            img.save(resized_filename)

        message = user + " hat jetzt " + album + " von " + band + "\n"

        tracks = MediaAgent.get_tracks(spotify=spotify, band=band, album=album)

        for track in tracks:
            message += track + "\n"

        if img is None:
            bot.send_message(chat_id=music_channel, text=message)
        else:
            bot.send_photo(chat_id=music_channel, photo=open(resized_filename, 'rb'), caption=message)

    @staticmethod
    def post_new_movie(movie_channel, movie_telegram_lock, messages, user: str, movie: Movie, bot: Bot):
        with movie_telegram_lock:
            message = f"{movie.title} ({movie.year})\n"
            message += messages[random.randint(0, len(messages) - 1)].format(user) + "\n\n"

            message += f"{movie.resolution} {movie.dynamic_range}\n"
            message += f"{format(movie.size / 1e9, '.2f')} GB - {format(movie.bitrate / 1e6, '.2f')} Mb/s\n"

            def language_name(code: str):
                code = code.strip()
                l = Locale.parse(code)
                return l.get_language_name(code)

            def clean_up(langs: []):
                res = []
                for lang in langs:
                    if lang != "":
                        res.append(lang.strip())
                return res

            audio_languages = clean_up(movie.audio_languages.split(','))
            subtitle_languages = clean_up(movie.subtitle_languages.split(','))

            if len(audio_languages) > 0:
                message += f"\nTonspuren:\n"
                for lang in audio_languages:
                    message += f"        {language_name(lang)}\n"
            if len(subtitle_languages) > 0:
                message += f"\nUntertitel:\n"
                for lang in subtitle_languages:
                    message += f"        {language_name(lang)}\n"
            try:
                movie = tmdb.Movies(id=movie.tmdb_id)
                info = movie.info()
                if info['poster_path'] is not None:
                    image_url = info['poster_path']
                    bot.send_photo(chat_id=movie_channel, caption=message, photo=f"https://image.tmdb.org/t/p/w500{image_url}")
                else:
                    bot.send_message(chat_id=movie_channel, text=message)
            except ConnectionError as e:
                print(f"ConnectionError: {e.msg}")
                return False
        return True

    @staticmethod
    def delete_downloads():
        for downloads in os.listdir(".\\downloads"):
            dir = ".\\downloads\\" + downloads
            shutil.rmtree(dir)

    @staticmethod
    def get_album_cover(band, album):
        response = google_images_download.googleimagesdownload()  # class instantiation
        key = "CD Album Cover Front " + band + " " + album
        arguments = {"keywords": key, "limit": 1, "print_urls": True}  # creating list of arguments
        paths = response.download(arguments)  # passing the arguments to the function
        return paths[0][key]

    @staticmethod
    def handle_album(bot, user, band, album):
        if not MediaAgent.exists_album(band, album):
            try:
                MediaAgent.post_new_album(user, band, album, bot)
            except Exception as e:
                print("Exception", e)
            MediaAgent.add_album(band, album)

    @staticmethod
    def handle_movie(database, movie_channel, movie_telegram_lock, messages, bot, user, movie: Movie, publish: bool):
        if not database.movie_exists(movie=movie):
            print(f"{movie} does not exist yet")
            add_movie = True
            if publish:
                max_retries = 5
                retries = 0
                posted = False
                while not posted and retries < max_retries:
                    retries = retries + 1
                    posted = MediaAgent.post_new_movie(
                        movie_channel=movie_channel,
                        movie_telegram_lock=movie_telegram_lock,
                        messages=messages,
                        user=user,
                        movie=movie,
                        bot=bot
                    )
                    if not posted:
                        time.sleep(2)
                add_movie = posted
            if add_movie:
                if not database.add_movie(user=user, movie=movie):
                    return False
        return True

    def init(self):
        self.config = self.load_config(self.dir+"config.txt")

        settings = [
            "certfile",
            "port",
            "tmdb_api_key",
            "telegram_token",
            "telegram_music_channel",
            "telegram_movie_channel",
            "spotify_client_id",
            "spotify_secret",
            "mariadb_user",
            "mariadb_pwd",
            "mariadb_host",
            "mariadb_port",
        ]

        failed = False

        for setting in settings:
            if self.config[setting] is None:
                print(f"{setting} is missing")
                failed = True

        if failed:
            return False

        print(f"{__name__} {__version__}\n")

        print("Loaded config:\n")
        for key in self.config:
            print(f"\t{key}:\n\t\t{self.config[key]}")
        print("")

        self.database = Database(
            user=self.config["mariadb_user"],
            pwd=self.config["mariadb_pwd"],
            host=self.config["mariadb_host"],
            port=self.config["mariadb_port"],
        )
        self.database.connect()
        self.database.create()

        self.connection = sqlite3.connect(self.dir + "db.db")
        self.cursor = self.connection.cursor()

        return True

    def serve(self):
        tmdb.API_KEY = self.config["tmdb_api_key"]
        self.search = tmdb.Search()
        self.token = self.config["telegram_token"]
        self.music_channel = self.config["telegram_music_channel"]
        self.movie_channel = self.config["telegram_movie_channel"]

        self.updater = Updater(self.token, use_context=True)
        self.client_credentials_manager = SpotifyClientCredentials(client_id=self.config["spotify_client_id"],
                                                                   client_secret=self.config["spotify_secret"])
        self.sp = spotipy.Spotify(client_credentials_manager=self.client_credentials_manager)
        self.sp.trace = False
        self.movie_telegram_lock = threading.Lock()

        if self.config["messages"] is not None:
            self.messages = self.config["messages"]

        server_address = ('', self.config["port"])
        httpd = HTTPServer(server_address, make_custom_handler(
            database=self.database,
            bot=self.updater.bot,
            movie_channel=self.movie_channel,
            messages=self.messages,
            movie_telegram_lock=self.movie_telegram_lock
        ))
        httpd.socket = ssl.wrap_socket(httpd.socket, certfile=self.dir+self.config["certfile"], server_side=True)
        print(f'Listening for https messages on {self.config["port"]}')
        httpd.serve_forever()

    def migrate(self):
        print("Migrating...")
        self.database.copy_from_sqlite(self.dir + "db.db")
        print("Migration... Done")

    def add_token(self):
        token = input("Enter a token: ")
        self.database.add_token(token)
