import argparse
from MediaAgent.MediaAgent import MediaAgent
import logging


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-a", "--add_token", action="store_true")
    parser.add_argument("-m", "--migrate", action="store_true")
    parser.add_argument("-s", "--serve", action="store_true")
    arguments = parser.parse_args()

    agent = MediaAgent()
    init_successful = agent.init()

    if not init_successful:
        logging.error("Failed to initialize")
        return

    if arguments.add_token:
        agent.add_token()
    if arguments.migrate:
        agent.migrate()
    if arguments.serve:
        agent.serve()


if __name__ == "__main__":
    main()
