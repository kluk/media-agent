from dataclasses import dataclass
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class Movie:
    title: str
    year: int
    size: int
    hash: str
    tmdb_id: int
    audio_languages: str
    subtitle_languages: str
    resolution: str
    dynamic_range: str
    bitrate: int
    sent: bool
