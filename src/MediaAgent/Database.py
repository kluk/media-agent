import sqlite3

import mariadb
import sys
import logging
from MediaAgent.Movie import Movie


class Database:

    def __init__(self, host: str, port: int, user: str, pwd: str):
        self.connection = None
        self.cursor = None
        self.host = host
        self.port = port
        self.user = user
        self.pwd = pwd

    def create_table_tokens(self):
        sql_command = """
           CREATE TABLE IF NOT EXISTS tokens ( 
           token VARCHAR(512),
           PRIMARY KEY(token),
           CONSTRAINT uid UNIQUE(token)
           );"""
        self.reconnect()
        self.cursor.execute(sql_command)

    def create_table_music(self):
        sql_command = """
        CREATE TABLE IF NOT EXISTS music ( 
        artist VARCHAR(320), 
        album VARCHAR(320),
        PRIMARY KEY(artist, album),
        CONSTRAINT uid UNIQUE(artist, album)
        );"""
        self.reconnect()
        self.cursor.execute(sql_command)

    def create_table_movies(self):
        sql_command = """
        CREATE TABLE IF NOT EXISTS movies ( 
        title VARCHAR(320), 
        year INTEGER,
        size BIGINT,
        hash VARCHAR(320),
        tmdb_id BIGINT,
        audio_languages VARCHAR(320),
        subtitle_languages VARCHAR(320),
        resolution VARCHAR(320),
        dynamic_range VARCHAR(320),
        bitrate BIGINT,
        user VARCHAR(320),
        PRIMARY KEY(title, year, size, hash),
        CONSTRAINT uid UNIQUE(title, year, size, hash)
        );"""
        self.reconnect()
        self.cursor.execute(sql_command)

    def token_exists(self, token: str):
        sql_command = f'SELECT token FROM tokens WHERE token="{token}";'
        self.reconnect()
        self.cursor.execute(sql_command)
        result = self.cursor.fetchall()
        return len(result) > 0

    def movie_exists(self, movie: Movie):
        sql_command = f'SELECT title FROM movies WHERE ' \
                      f'hash="{movie.hash}" ' \
                      f'OR (title="{movie.title}" AND year="{movie.year}");'
        if not self.reconnect():
            return False
        try:
            self.cursor.execute(sql_command)
            result = self.cursor.fetchall()
        except mariadb.Error as e:
            logging.error(f"{e}")
            return False
        return len(result) > 0

    def album_exists(self, band, album):
        sql_command = 'SELECT * FROM music WHERE artist="' + band + '" AND album="' + album + '";'
        self.reconnect()
        self.cursor.execute(sql_command)
        result = self.cursor.fetchall()
        return len(result) > 0

    def add_token(self, token: str):
        if self.token_exists(token=token):
            return

        sql_command = f'INSERT INTO tokens (token) VALUES ("{token}");'
        self.reconnect()
        self.cursor.execute(sql_command)
        self.connection.commit()

    def add_album(self, band, album):
        if self.album_exists(band=band, album=album):
            return True

        sql_command = 'INSERT INTO music (artist, album) VALUES ("' + band + '", "' + album + '");'
        self.reconnect()
        self.cursor.execute(sql_command)
        self.connection.commit()
        return True

    def add_movie(self, user: str, movie: Movie):
        if self.movie_exists(movie=movie):
            return True

        sql_command = f'INSERT INTO movies (title, year, size, hash, tmdb_id, audio_languages, ' \
                      f'subtitle_languages, resolution, dynamic_range, bitrate, user) ' \
                      f'VALUES ("{movie.title}", {movie.year}, {movie.size}, "{movie.hash}", ' \
                      f'{movie.tmdb_id}, "{movie.audio_languages}", "{movie.subtitle_languages}", ' \
                      f'"{movie.resolution}", "{movie.dynamic_range}", {movie.bitrate}, "{user}");'
        if not self.reconnect():
            logging.error("The database has gone away")
            return False
        try:
            self.cursor.execute(sql_command)
            self.connection.commit()
        except mariadb.Error as e:
            logging.error(f"The movie {movie.title} could not be added: {e}")
            return False
        return True

    def reconnect(self):
        try:
            self.connection.ping()
        except mariadb.Error as e:
            logging.error(f"{e}")
        else:
            return True

        logging.warning("The database has gone away -- reconnecting.")
        try:
            self.connection.reconnect()
        except mariadb.Error as e:
            logging.error(f"The database could not be reconnected: {e}")
            return False

        logging.warning("The database was reconnected.")
        return True

    def connect(self):
        try:
            self.connection = mariadb.connect(
                user=self.user,
                password=self.pwd,
                host=self.host,
                port=self.port,
                database="media_agent",
            )
        except mariadb.Error as e:
            logging.error(f"Error connecting to MariaDB Platform: {e}")
            sys.exit(1)

        # Get Cursor
        self.cursor = self.connection.cursor()

    def create(self):
        self.create_table_tokens()
        self.create_table_music()
        self.create_table_movies()
        self.connection.commit()

    def copy_from_sqlite(self, file: str):
        self.create()
        connection = sqlite3.connect(file)
        cursor = connection.cursor()

        cursor.execute("SELECT token FROM tokens;")
        tokens = cursor.fetchall()

        for token, in tokens:
            self.add_token(token=token)

        cursor.execute("SELECT title, year, size, hash, tmdb_id, audio_languages, "\
                       "subtitle_languages, resolution, dynamic_range, bitrate, user FROM movies;")
        movies = cursor.fetchall()

        for title, year, size, hash, tmdb_id, audio_languages, \
            subtitle_languages, resolution, dynamic_range, bitrate, user in movies:
            movie_entity = Movie(
                title=title,
                year=year,
                size=size,
                hash=hash,
                tmdb_id=tmdb_id,
                audio_languages=audio_languages,
                subtitle_languages=subtitle_languages,
                resolution=resolution,
                dynamic_range=dynamic_range,
                bitrate=bitrate,
                sent=True,
            )
            self.add_movie(user=user, movie=movie_entity)
