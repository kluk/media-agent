FROM python:3.9
COPY . /app
RUN cd /app && python -m pip install -e .
CMD ["media_agent", "--serve"]
