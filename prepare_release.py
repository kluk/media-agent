#! /usr/bin/env python
"""
    Prepare everything for a release of the software.

    1. update the version number in `version`
    2. (Re)build the code to include the new version
    3. Generate packages

    The updated version is passed via the command line. This script expects to be called from the
    repository root.

    Usage: prepare_release.py <Major.Minor.Patch>

    The script is run by semantic-release in the CI pipeline.
"""

import sys
import subprocess

VERSION_FILE = "src/MediaAgent/__init__.py"
BUILD_FOLDER = "bin"


def main():
    if len(sys.argv) < 2:
        print(__doc__)
        return 1

    new_version = sys.argv[1]

    print("Update version file to", new_version)
    with open(VERSION_FILE, 'w') as file:
        lines = [
            f'__all__ = ["MediaAgent", "__version__", "__name__", "__author__"]\n',
            f'\n',
            f'__version__ = "{new_version}"\n',
            f'__name__ = "MediaAgent"\n',
            f'__author__ = "Lukas Riedersberger"\n',
        ]
        for line in lines:
            file.write(line)

    print("Build with new version")
    subprocess.run(["python", "-m", "pip", "install", "-e", "."])
    subprocess.run(["python", "setup.py", "sdist", "bdist_wheel"])

    return 0


if __name__ == "__main__":
    sys.exit(main())
