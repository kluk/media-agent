## [1.1.2](https://gitlab.com/kluk/media-agent/compare/v1.1.1...v1.1.2) (2021-09-11)


### Bug Fixes

* duplicate movies are now detected ([ed4c2bd](https://gitlab.com/kluk/media-agent/commit/ed4c2bd49f9c9f72633f82dfaed4158512c3e6d0))

## [1.1.1](https://gitlab.com/kluk/media-agent/compare/v1.1.0...v1.1.1) (2021-01-30)


### Bug Fixes

* reconnect to mariadb ([2968ffc](https://gitlab.com/kluk/media-agent/commit/2968ffce1b6512b6b971af33fb19fb2f07745c54)), closes [#6](https://gitlab.com/kluk/media-agent/issues/6)
* respond with 503 when a movie could not be added ([560419c](https://gitlab.com/kluk/media-agent/commit/560419c9a8237e79bf5794f436b41d25d4e1592a)), closes [#7](https://gitlab.com/kluk/media-agent/issues/7)

# [1.1.0](https://gitlab.com/kluk/media-agent/compare/v1.0.0...v1.1.0) (2021-01-10)


### Bug Fixes

* automatically reconnect to mariadb ([2d84756](https://gitlab.com/kluk/media-agent/commit/2d847560dcae50ea9e86795e85d0fb6894d9efe5)), closes [#5](https://gitlab.com/kluk/media-agent/issues/5)


### Features

* use mariadb ([cd37875](https://gitlab.com/kluk/media-agent/commit/cd37875b4b5c0abe7b6401b9063ac72c339633e7))

# 1.0.0 (2020-12-28)


### Features

* CD with semantic release ([773b493](https://gitlab.com/kluk/media-agent/commit/773b493c4068fc662b071626dae9b948de6eb1cb)), closes [#3](https://gitlab.com/kluk/media-agent/issues/3)
* initial release of media agent ([87c8dec](https://gitlab.com/kluk/media-agent/commit/87c8dec28679cb301cb31c827fef45ac780bcde2))


### BREAKING CHANGES

* This is the first release of the media agent.
