import setuptools


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

media_agent_namespace = {}
media_agent_init_path = setuptools.convert_path('src/MediaAgent/__init__.py')
with open(media_agent_init_path) as ver_file:
    exec(ver_file.read(), media_agent_namespace)

setuptools.setup(
    name=media_agent_namespace["__name__"],
    version=media_agent_namespace["__version__"],
    author=media_agent_namespace["__author__"],
    author_email="lukas.riedersberger@posteo.de",
    description="short description",
    url="https://gitlab.com/kluk/media-agent",
    package_dir={'': 'src'},
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(where="src"),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.9',
    install_requires=[
        "telegram",
        "python-telegram-bot==12.7",
        "google_images_download",
        "pysqlite3",
        "pillow",
        "spotipy",
        "tmdbsimple",
        "dataclasses-json",
        "Babel",
        "wheel",
        "urllib3",
        "chardet",
        "mariadb",
    ],
    include_package_data=True,
    entry_points={
      'console_scripts': [
          'media_agent = MediaAgent.__main__:main'
      ]
    },
)